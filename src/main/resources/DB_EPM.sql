#namespace("sys_account")
	#sql("admin-paginate")
		select a.id,a.name,a.username,a.lastLoginAt,a.roleId,b.name as roleName,a.ip,a.status,a.createAt,a.updateAt from sys_account a
		left join sys_role b on a.roleId = b.id
		where 1 = 1
		#if(sk.notBlank(name))
			and a.name = #p(name)
		#end
		#if(sk.notBlank(username))
			and a.username = #p(username)
		#end
	#end
#end

#namespace("depart")
	#sql("paginate")
		select a.id,a.dep_name,b.name as dep_manager ,a.dep_remark,a.createAt from depart as a
		left join employees b on a.dep_manager = b.id
		where 1 = 1
		#if(sk.notBlank(name))
			and a.dep_name = #p(name)
		#end
	#end
#end

#namespace("employees")
    #sql("paginate")
		select a.id,a.name,a.sex,b.dep_name as d_id ,a.createAt,a.post,a.entrytime,a.education,a.createAt from employees a
		left join depart b on a.d_id = b.id
		where 1 = 1
		#if(sk.notBlank(name))
			and a.name = #p(name)
		#end
		order by a.id
	#end
#end

#namespace("sys_role")
    #sql("paginate")
		select * from sys_role as a
		where 1 = 1
		#if(sk.notBlank(name))
			and a.name = #p(name)
		#end
	#end
#end

#namespace("sys_rule")
    #sql("paginate")
		select * from sys_menu as a
		where 1 = 1
		#if(sk.notBlank(name))
			and a.name = #p(name)
		#end
	#end
#end

#namespace("target")
    #sql("paginate")
		select a.id,target,score,a.createAt,b.name as creator from assessment_target as a
		left join sys_account b on a.creator=b.id
		where 1 = 1
		#if(sk.notBlank(target))
			and a.target = #p(target)
		#end
	#end
#end

#namespace("stand")
    #sql("paginate")
		select * from assessment_standard as a
		where 1 = 1 and t_id = #p(t_id)
		#if(sk.notBlank(standard))
			and a.standard = #p(standard)
		#end
	#end
#end