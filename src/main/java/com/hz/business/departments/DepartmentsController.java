package com.hz.business.departments;

import com.alibaba.fastjson.JSON;
import com.hz.common.controller.AdminController;
import com.hz.common.controller.BaseController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Depart;
import com.hz.common.model.Employees;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;

import java.util.Date;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:17
 */
public class DepartmentsController extends AdminController {
    static final DepartmentService srv = DepartmentService.me;

    public void index() {
        setAttr("employees", Employees.dao.findAll());
        render("/pages/personnel/depart.html");
    }
    public void add() {
        render("/pages/personnel/department/add.html");
    }

    public void edit() {
        setAttr("department",Depart.dao.findById(getParaToInt("id")));
        render("/pages/personnel/depart.html");
    }

    public void update(){
        String json = HttpKit.readData(getRequest());
        Depart depart = JSON.parseObject(json, Depart.class);
        renderJson(srv.update(depart));
    }
    public void list() {
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }

    public void save(){
        String json = HttpKit.readData(getRequest());
        Depart depart = JSON.parseObject(json, Depart.class);
        depart.setCreateAt(new Date());
        renderJson(srv.save(depart));
    }

    public void remove(){
        int id = getParamToInt("id");
        renderJson(srv.remove(id));
    }
}
