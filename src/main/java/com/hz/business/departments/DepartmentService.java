package com.hz.business.departments;

import com.hz.common.controller.BaseController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Depart;
import com.hz.common.model.Employees;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:17
 */
public class DepartmentService extends BaseController {

    public static final DepartmentService me = new DepartmentService();
    static final Depart dao = Depart.dao;

    public Page<Depart> paginate(int pageNumber, int pageSize, Kv kv) {
        Page<Depart> page = dao.paginate(pageNumber, pageSize, dao.getSqlPara("depart.paginate", kv));
        return page;
    }

    public RetKit update(Depart depart) {
        return depart.update() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit save(Depart depart) {
        return depart.save() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit remove(int id) {
        List<Employees> employees = Employees.dao.find("select * from employees where d_id=?", id);
        if (employees.size() > 0) {
            return RetKit.fail("该部门下存在员工，暂时无法删除");
        } else {
            return dao.deleteById(id) ? RetKit.ok() : RetKit.fail();
        }
    }
}
