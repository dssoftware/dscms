package com.hz.business.admin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hz.common.controller.AdminController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Account;
import com.hz.common.model.Depart;
import com.hz.common.model.Role;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;


public class AccountListController extends AdminController {

    static final AccountListService srv = AccountListService.me;

    @Override
    public void index() {
        render("/pages/admin/list.html");
    }

    public void add() {
        setAttr("departments", Depart.dao.findAll());
        render("/pages/admin/add.html");
    }

    public void edit() {
        setAttr("account",Account.dao.findById(getParaToInt("id")));
        setAttr("roles",Role.dao.findAll());
        setAttr("departments", Depart.dao.findAll());
        render("/pages/admin/edit.html");
    }

    public void list() {
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }

    public void updateStatus() {
        String json = HttpKit.readData(getRequest());
        JSONObject reqJson = JSON.parseObject(json);
        int status = reqJson.getIntValue("status");
        int id = reqJson.getIntValue("id");
        renderJson(srv.update(new Account().setId(id).setStatus(status),getSessionId()));
    }

    public void remove() {
        Integer id = getParaToInt("id");
        renderJson(srv.remove(id, getAccount()));
    }

    public void update() {
        String json = HttpKit.readData(getRequest());
        Account account = JSON.parseObject(json,Account.class);
        renderJson(srv.update(account,getSessionId()));
    }

    public void save() {
		String json = HttpKit.readData(getRequest());
		Account account = JSON.parseObject(json,Account.class);
        renderJson(srv.save(account));
    }

    public void getRoleList() {
        renderJson(RetKit.ok("list", srv.getRoleList()));
    }


}
