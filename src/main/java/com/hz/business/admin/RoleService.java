package com.hz.business.admin;

import com.hz.common.constant.CacheConstant;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Menu;
import com.hz.common.model.Role;
import com.hz.common.model.RoleMenu;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RoleService {

    public static final RoleService me = new RoleService();
    static final Menu menuDao = Menu.dao;
    static final RoleMenu roleMenuDao = RoleMenu.dao;
    static final Role roleDao = Role.dao;


    public Page<Role> paginate(int pageNumber, int pageSize, Kv kv) {
        Page<Role> page = roleDao.paginate(pageNumber, pageSize, roleDao.getSqlPara("sys_role.paginate", kv));
        return page;
    }

    public Page<Menu> paginaterule(int pageNumber, int pageSize, Kv kv) {
        Page<Menu> page = Menu.dao.paginate(pageNumber, pageSize, roleDao.getSqlPara("sys_rule.paginate", kv));
        return page;
    }

    public List<Menu> findMenuIdsByRole(Integer roleId) {
        return menuDao.find(
                "select * from sys_menu where status = ? and id in (select menuId from sys_role_menu where roleId = ? ) order by level,sort",
                Menu.STATUS_OK, roleId);
    }

    public List<Menu> findMenuByRole(Integer roleId) {
        List<Menu> menuList = new ArrayList<>();
        if (roleId == 0) {
            menuList = menuDao.find("select a.id,a.name as title from sys_menu as a where status = ? and isParent=1 order by level,sort",
                    Menu.STATUS_OK);
            for (Menu m : menuList
            ) {
                List<Menu> children = menuDao.find("select a.id,a.name as title from sys_menu a where status = ? and parentId =? order by level,sort",
                        Menu.STATUS_OK, m.getId());
                m.put("children", children);
            }
        } else {
            menuList = menuDao.find(
                    "select a.id from sys_menu a where status = ? and id in (select menuId from sys_role_menu where roleId = ? ) order by level,sort",
                    Menu.STATUS_OK, roleId);
        }
        return menuList;
    }

    public Object[] findMenuNamesByRole(Integer roleId) {
        List<Menu> menuList = new ArrayList<>();
        // 如果是超管
        if (roleId == 0) {
            menuList = menuDao.find("select name from sys_menu where status = ? order by level,sort",
                    Menu.STATUS_OK);
        } else {
            menuList = menuDao.find(
                    "select name from sys_menu where status = ? and id in (select menuId from sys_role_menu where roleId = ? ) order by level,sort",
                    Menu.STATUS_OK, roleId);
        }
        return menuList.toArray();
    }

    public List<Menu> findMenuListByRoleId(Integer roleId) {
        List<Menu> menuList = CacheKit.get(CacheConstant.SYS_ROLE_MENU, roleId);
        if (menuList == null) {
            if (roleId == 1) {
                menuList = menuDao.find("select * from sys_menu where status = ? and isParent=1 order by level,sort",
                        Menu.STATUS_OK);
                for (Menu m : menuList
                ) {
                    List<Menu> children = menuDao.find("select * from sys_menu where status = ? and parentId =? order by level,sort",
                            Menu.STATUS_OK, m.getId());
                    m.put("children", children);
                }
            } else {
                menuList = menuDao.find(
                        "select * from sys_menu where status = ? and isParent=1 and id in (select menuId from sys_role_menu where roleId = ? ) order by level,sort",
                        Menu.STATUS_OK, roleId);
                for (Menu m : menuList
                ) {
                    List<Menu> children = menuDao.find("select * from sys_menu where status = ? and parentId =? and id in (select menuId from sys_role_menu where roleId = ? ) order by level,sort",
                            Menu.STATUS_OK, m.getId(),roleId);
                    m.put("children", children);
                }
            }
            CacheKit.put(CacheConstant.SYS_ROLE_MENU, roleId, menuList);
        }
        return menuList;
    }

    public List<Kv> findMenus() {
        List<Menu> menuList = menuDao.find("select * from sys_menu where status = ? order by level,sort",
                Menu.STATUS_OK);
        return getChildrenMenu(menuList, 0);
    }

    public List<Kv> getChildrenMenu(List<Menu> list, Integer parentId) {
        List<Kv> kvList = new ArrayList<>();
        for (Menu sysMenu : list) {
            if (parentId > 0) {
                Kv kv = new Kv();
                if (sysMenu.getParentId().equals(parentId)) {
                    kv.set("id", sysMenu.getId());
                    kv.set("label", sysMenu.getName());
                    if (sysMenu.getIsParent()) {
                        kv.set("children", getChildrenMenu(list, sysMenu.getId()));
                    }
                    kvList.add(kv);
                }
            } else if (sysMenu.getIsParent()) {
                Kv kv = new Kv();
                kv.set("id", sysMenu.getId());
                kv.set("label", sysMenu.getName());
                kv.set("children", getChildrenMenu(list, sysMenu.getId()));
                kvList.add(kv);
            }
        }
        return kvList;
    }

    public List<Role> list() {
        List<Role> list = roleDao.find("select a.*,(select count(*) from sys_account b where a.id = b.roleId ) as roleAccount from sys_role a order by createAt desc");
        for (Role role : list) {
            role.put("menus", findMenuIdsByRole(role.getId()));
        }
        return list;
    }

    public RetKit saveRole(Role role) {
        role.setCreateAt(new Date()).setUpdateAt(new Date());
        boolean succ = role.save();
        return succ ? RetKit.ok() : RetKit.fail();

    }

    public RetKit save(List<String> menus, String name) {
        if (isExistName(name)) {
            return RetKit.fail("權限組名稱已存在");
        }
        boolean succ = Db.tx(() -> {
            Role role = new Role();
            role.setName(name);
            role.setCreateAt(new Date());
            role.setUpdateAt(new Date());
            boolean succ1 = role.save();
            if (!succ1) {
                return false;
            }
            List<RoleMenu> list = new ArrayList<>();
            for (String menuId : menus) {
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setMenuId(Integer.valueOf(menuId));
                roleMenu.setRoleId(role.getId());
                roleMenu.setCreateAt(new Date());
                list.add(roleMenu);
            }
            Db.batchSave(list, list.size());
            return true;
        });
        return succ ? RetKit.ok() : RetKit.fail();

    }

    public RetKit update(Role role) {
        role.setUpdateAt(new Date());
        boolean succ = role.update();
        return succ ? RetKit.ok() : RetKit.fail();
    }

    public RetKit updateMenu(List<String> menus, Integer id) {
        Role role = Role.dao.findById(id);
        role.setUpdateAt(new Date());
        boolean succ = role.update();
        // 先删除再插入
        deleteRoleMenuByRoleId(role.getId());
        List<RoleMenu> list = new ArrayList<>();
        for (String menuId : menus) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(Integer.parseInt(menuId));
            roleMenu.setRoleId(role.getId());
            roleMenu.setCreateAt(new Date());
            list.add(roleMenu);
        }
        Db.batchSave(list, list.size());
        //更新缓存
        CacheKit.remove(CacheConstant.SYS_ROLE_MENU, role.getId());
        return succ ? RetKit.ok() : RetKit.fail();
    }

    private boolean deleteRoleMenuByRoleId(long id) {
        return Db.update("delete from sys_role_menu where roleId = ?", id) > 1;
    }

    public RetKit remove(Integer id) {
        if (isExistAccountRole(id)) {
            return RetKit.fail("該權限組被使用中，不能刪除");
        }
        boolean succ = roleDao.deleteById(id);
        deleteRoleMenuByRoleId(id);
        //更新缓存
        CacheKit.remove(CacheConstant.SYS_ROLE_MENU, id);
        return succ ? RetKit.ok() : RetKit.fail();
    }

    public boolean isExistAccountRole(Integer id) {
        return Db.queryInt("select id from sys_account where roleId = ? limit 1", id) != null;
    }

    public boolean isExistName(String name) {
        return Db.queryInt("select id from sys_account where name = ? limit 1", name.trim()) != null;
    }

    public boolean isExistName(String name, Integer id) {
        return Db.queryInt("select id from sys_account where name = ? and id != ? limit 1", name.trim(), id) != null;
    }

    public RetKit MenuSave(Menu menu){
        if(menu.getParentId()==0){
            menu.setIsParent(true);
            menu.setLevel(1);
        }else {
            menu.setIsParent(false);
            menu.setLevel(2);
        }
        menu.setCreateAt(new Date());
        return menu.save()?RetKit.ok():RetKit.fail();
    }

    public RetKit MenuUpdate(Menu menu){
        return menu.update()?RetKit.ok():RetKit.fail();
    }
    public RetKit MenuRemove(Integer menuid){
        boolean succ = Db.tx(() -> {
            Db.update("delete from sys_menu where parentId=?",menuid);
            return Menu.dao.deleteById(menuid);
        });
        return succ?RetKit.ok():RetKit.fail();
    }
}
