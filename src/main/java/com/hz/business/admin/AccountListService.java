package com.hz.business.admin;

import com.hz.business.login.LoginService;
import com.hz.common.constant.CacheConstant;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Account;
import com.hz.common.model.Role;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.Date;
import java.util.List;

public class AccountListService {

	public static final	AccountListService me = new AccountListService();
	static final Account dao = Account.dao;
	
	public Page<Account> paginate(int pageNumber, int pageSize, Kv kv) {
		Page<Account> page = dao.paginate(pageNumber, pageSize, dao.getSqlPara("sys_account.admin-paginate", kv));
		for (Account account : page.getList()) {
			if (account.isSysAdmin()) {
				account.put("roleName", "超級管理員");
			}
		}
		return page;
	}
	
	public boolean isExistAccount(String username){
		return Db.queryInt("select id from sys_account where username = ? limit 1",username) != null;
	}

	public Account findById(Integer id){
		return dao.findById(id);
	}
	
	public RetKit save(Account account){
		if (account == null || account.getRoleId() <= 0) {
			return RetKit.fail("非法參數");
		}
		if (isExistAccount(account.getUsername().trim())) {
			return RetKit.fail("登陸賬號已存在");
		}
		// 密码加盐 hash
		String salt = HashKit.generateSaltForSha256();
		String password = HashKit.sha256(salt + account.getPassword());
		account.setUsername(account.getUsername().trim());
		account.setSalt(salt);
		account.setPassword(password);
		account.setCreateAt(new Date());
		account.setUpdateAt(new Date());
		boolean succ = account.save();
		return succ ? RetKit.ok() : RetKit.fail();
	}
	
	public RetKit update(Account account,String sessionid){
		Account tempAccount = LoginService.me.getAccountCacheWithSessionId(sessionid);
		//如果改了登陆账号
		if (tempAccount.getUsername().equals(account.getUsername())) {
			//更新缓存
			CacheKit.put(CacheConstant.SYS_ACCOUNT,sessionid,account);
		}
		account.setUpdateAt(new Date());
		boolean succ = account.update();
		return succ ? RetKit.ok() : RetKit.fail();
	}

	public RetKit remove(Integer id,Account loginAccount) {
		Account account = findById(id);
		if (account.isSysAdmin() ) {
			return RetKit.fail("不允許刪除系統管理員");
		}
		if (loginAccount.getId().equals(id)) {
			return RetKit.fail("不允許自己刪除自己");
		}
		boolean succ = account.delete();
		return succ ? RetKit.ok() : RetKit.fail();
	}
	
	public List<Role> getRoleList(){
		return RoleService.me.list();
	}

	
	
}
