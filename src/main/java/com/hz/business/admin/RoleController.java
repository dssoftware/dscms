package com.hz.business.admin;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hz.common.controller.AdminController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Menu;
import com.hz.common.model.Role;
import com.jfinal.aop.Clear;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;

import java.util.List;

public class RoleController extends AdminController {

    static final RoleService srv = RoleService.me;

    public void index() {
        render("/pages/admin/role.html");
    }

    public void rule() {
        setAttr("parents", Menu.dao.find("select * from sys_menu where isParent=1"));
        render("/pages/admin/rule.html");
    }

    public void getParent(){
        renderJson("parents", Menu.dao.find("select * from sys_menu where isParent=1"));
    }
    @Clear
    public void findMenuByRole() {
        renderJson(RetKit.ok("data", srv.findMenuByRole(getParaToInt("roleid", 0))));
    }

    public void list() {
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }

    public void menuList() {
        int pageSize = getParaToInt("pageSize", 100);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginaterule(pageNumber, pageSize, kv)));
    }

    public void getRole() {
        renderJson(RetKit.ok("rolelist", new Role().dao().findAll()));
    }

    public void saveRole() {
        String json = HttpKit.readData(getRequest());
        Role role = JSON.parseObject(json, Role.class);
        renderJson(srv.saveRole(role));
    }

    public void remove() {
        Integer id = getParamToInt("id");
        renderJson(srv.remove(id));
    }

    public void update() {
        String json = HttpKit.readData(getRequest());
        Role role = JSON.parseObject(json, Role.class);
        renderJson(srv.update(role));
    }

    public void updateMenu() {
        JSONObject data = JSONObject.parseObject(getRawData());
        List<String> menus = JSONObject.parseArray(data.getJSONArray("menuids").toJSONString(), String.class);
        Integer id = data.getInteger("id");
        renderJson(srv.updateMenu(menus, id));
    }

    @Clear
    public void getMenuList() {
        renderJson(RetKit.ok("list", srv.findMenus()));
    }

    @Clear
    public void findMenuListByRoleId() {
        renderJson(RetKit.ok("data", srv.findMenuListByRoleId(getParaToInt("roleid"))));
    }

    public void menuSave(){
        String json = HttpKit.readData(getRequest());
        Menu menu = JSON.parseObject(json, Menu.class);
        renderJson(srv.MenuSave(menu));
    }

    public void menuUpdate(){
        String json = HttpKit.readData(getRequest());
        Menu menu = JSON.parseObject(json, Menu.class);
        renderJson(srv.MenuUpdate(menu));
    }

    public void menuRemove(){
        int menuid = getParamToInt("id");
        renderJson(srv.MenuRemove(menuid));
    }
}
