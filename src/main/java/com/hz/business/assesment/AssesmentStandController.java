package com.hz.business.assesment;

import com.alibaba.fastjson.JSON;
import com.hz.business.login.LoginService;
import com.hz.common.controller.AdminController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.AssessmentStandard;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;

import java.util.Date;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:21
 */
public class AssesmentStandController extends AdminController {

    static final AssesmentStandService srv = AssesmentStandService.me;

    public void list(){
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }

    public void save(){
        String json = HttpKit.readData(getRequest());
        AssessmentStandard account = JSON.parseObject(json,AssessmentStandard.class);
        account.setCreateAt(new Date());
        account.setCreator(LoginService.me.getAccountCacheWithSessionId(getSessionId()).getId());
        renderJson(srv.save(account));
    }

    public void update(){
        String json = HttpKit.readData(getRequest());
        AssessmentStandard account = JSON.parseObject(json,AssessmentStandard.class);
        renderJson(srv.update(account));
    }

    public void remove(){
        int id = getParamToInt("id");
        renderJson(srv.remove(id));
    }
}
