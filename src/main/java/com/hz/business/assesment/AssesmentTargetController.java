package com.hz.business.assesment;

import com.alibaba.fastjson.JSONObject;
import com.hz.business.login.LoginService;
import com.hz.common.controller.AdminController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.AssessmentTarget;
import com.hz.common.model.Depart;
import com.jfinal.kit.Kv;
import org.apache.poi.ss.formula.functions.T;

import java.util.Date;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:23
 */
public class AssesmentTargetController extends AdminController {
    static final AssesmentTargetService srv = AssesmentTargetService.me;

    public void index(){
        setAttr("departments", Depart.dao.findAll());
        render("/pages/criterion/criterion.html");
    }

    public void list(){
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }

    public void save(){
        JSONObject json = JSONObject.parseObject(getRawData());
        AssessmentTarget target = new AssessmentTarget();
        target.setCreateAt(new Date());
        target.setCreator(LoginService.me.getAccountCacheWithSessionId(getSessionId()).getId());
        target.setScore(json.getIntValue("score"));
        target.setTarget(json.getString("target"));
        String[] departs = json.getString("dep_id").split(",");
        renderJson(srv.save(target,departs));
    }

    public void update(){
        JSONObject json = JSONObject.parseObject(getRawData());
        AssessmentTarget target = new AssessmentTarget();
        target.setId(json.getIntValue("id"));
        target.setCreator(LoginService.me.getAccountCacheWithSessionId(getSessionId()).getId());
        target.setScore(json.getIntValue("score"));
        target.setTarget(json.getString("target"));
        String[] departs = json.getString("dep_id").split(",");
        renderJson(srv.update(target,departs));
    }

    public void remove(){
        int id = getParamToInt("id");
        renderJson(srv.remove(id));
    }
}
