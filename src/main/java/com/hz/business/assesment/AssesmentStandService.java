package com.hz.business.assesment;

import com.hz.common.kit.RetKit;
import com.hz.common.model.AssessmentStandard;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:24
 */
public class AssesmentStandService {

    public static final AssesmentStandService me = new AssesmentStandService();
    static final AssessmentStandard dao = AssessmentStandard.dao;

    public Page<AssessmentStandard> paginate(int pageNumber, int pageSize, Kv kv) {
        Page<AssessmentStandard> page = dao.paginate(pageNumber, pageSize, dao.getSqlPara("stand.paginate", kv));
        return page;
    }

    public RetKit save(AssessmentStandard standard) {
        return standard.save() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit update(AssessmentStandard standard) {
        return standard.update() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit remove(int id) {
        return AssessmentStandard.dao.deleteById(id) ? RetKit.ok() : RetKit.fail();
    }
}
