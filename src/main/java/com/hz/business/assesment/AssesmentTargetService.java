package com.hz.business.assesment;

import com.hz.common.kit.RetKit;
import com.hz.common.model.AssessmentTarget;
import com.hz.common.model.TargetDepart;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.lang.annotation.Target;
import java.util.List;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:23
 */
public class AssesmentTargetService {

    public static final AssesmentTargetService me = new AssesmentTargetService();
    static final AssessmentTarget dao = AssessmentTarget.dao;

    public Page<AssessmentTarget> paginate(int pageNumber, int pageSize, Kv kv) {
        Page<AssessmentTarget> page = dao.paginate(pageNumber, pageSize, dao.getSqlPara("target.paginate", kv));
        List<AssessmentTarget> targets = page.getList();
        for (AssessmentTarget target : targets) {
            List<Record> record = Db.find("select id,dep_name from depart where id in (select depid from target_depart where targetid =?)",target.getId());
            target.put("departs",record);
        }
        return page;
    }

    public RetKit save(AssessmentTarget target, String[] departs) {
        boolean succ = Db.tx(() -> {
            target.save();
            for (String i : departs
            ) {
                new TargetDepart().setDepid(Integer.parseInt(i)).setTargetid(target.getId()).save();
            }
            return true;
        });
        return succ ? RetKit.ok() : RetKit.fail();
    }

    public RetKit update(AssessmentTarget target, String[] departs) {
        boolean succ = Db.tx(() -> {
            target.update();
            //先删除再绑定
            Db.update("delete from target_depart where targetid=?",target.getId());
            for (String i : departs
            ) {
                new TargetDepart().setDepid(Integer.parseInt(i)).setTargetid(target.getId()).save();
            }
            return true;
        });
        return succ ? RetKit.ok() : RetKit.fail();
    }

    public RetKit remove(int id){
        int size = Db.queryInt("select * from target_depart where targetid=?",id);
        if (size>0){
            return RetKit.fail("此考核目标有部门正在执行，无法删除，请解除绑定后重试");
        }else {
            boolean succ = Db.tx(() -> {
                AssessmentTarget.dao.deleteById(id);
                //删除对应的考核标准
                Db.update("delete from assessmentstandard where t_id=?",id);
                return true;
            });
            return succ ? RetKit.ok() : RetKit.fail();
        }
    }
}
