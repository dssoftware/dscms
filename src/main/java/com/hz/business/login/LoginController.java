package com.hz.business.login;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hz.common.controller.BaseController;
import com.hz.common.kit.RetKit;
import com.jfinal.aop.Clear;
import com.jfinal.kit.HttpKit;

/**
 * @author Xiaoy
 */
@Clear
public class LoginController extends BaseController {
	
	static final LoginService srv = LoginService.me;

	//默认方法
	public void index() {
		render("/login.html");
	}

	public void doLogin(){
		String json = HttpKit.readData(getRequest());
		JSONObject reqJson = JSON.parseObject(json);
		String username = reqJson.getString("username");
		String password = reqJson.getString("password");
		String loginIp = getIpAddress();
		String sessionid = getSession().getId();
		renderJson(srv.doLogin(username, password, loginIp,sessionid));
	}
	
	public void logout(){
		String sessionId  = getSession().getId();
		srv.logout(sessionId);
		renderJson(RetKit.ok());
	}
	
//	public void loginBySessionId(){
//		String sessionId = getHeader(Constant.ADMIN_SESSION_ID);
//		LogKit.info(getHeader(Constant.ADMIN_SESSION_ID));
//		Account account =  srv.getAccountCacheWithSessionId(sessionId);
//		if (account == null) {
//			account = srv.loginWithSessionId(sessionId, getIpAddress());
//		}
//		if (account == null) {
//			renderJson(RetKit.fail(RetConstant.CODE_LOGIN_EXPIRE, "Session失效，請重新登陸"));
//		}else{
//			renderJson(RetKit.ok("account",account));
//		}
//	}
}
