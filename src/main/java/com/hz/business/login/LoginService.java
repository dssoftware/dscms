package com.hz.business.login;

import com.hz.business.admin.RoleService;
import com.hz.common.constant.CacheConstant;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Account;
import com.hz.common.model.Session;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.Date;
import java.util.List;

public class LoginService {
	
	public static final LoginService me = new LoginService();
	static final Account accountDao = Account.dao;
	
	RetKit doLogin(String username, String password, String loginIp,String sessionid){
		if (StrKit.isBlank(username) || StrKit.isBlank(password)) {
			return RetKit.fail("用戶名或密碼不能為空");
		}
		username = username.trim();
		password = password.trim();
		Account account = accountDao.findFirst("select * from sys_account where username = ? limit 1", username);
		if (account == null) {
			return RetKit.fail("賬號不存在");
		}
		String salt = account.getSalt();
		String hashPassword = HashKit.sha256(salt + password);
		if (hashPassword.equals(account.getPassword())) {
			// 过期时间为7天，跟前端保持一致
//			long liveSeconds =  7 * 24 * 60 * 60;
			// expireAt 用于设置 session 的过期时间点，需要转换成毫秒
//			long expireAt = System.currentTimeMillis() + (liveSeconds * 1000);
			// 生成sessionId
//			String sessionId = HashKit.sha256(StrKit.getRandomUUID());
			// 保存登录 session 到数据库
//			Session session = new Session();
//			session.setId(sessionId);
//			session.setAccountId(account.getId());
//			session.setExpireAt(expireAt);
//			session.setCareateAt(new Date());
//			session.save();
			// 往Account放一份sessionId
//			account.put("sessionId", sessionId);
			account.remove("salt","password");
			//菜单数据
			account.put("menus", RoleService.me.findMenuNamesByRole(account.getRoleId()));
			account.setIp(loginIp);
			account.setLastLoginAt(new Date());
			account.update();
			//放进缓存
			CacheKit.put(CacheConstant.SYS_ACCOUNT, sessionid, account);
			return RetKit.ok("account",account);
		} else {
			return RetKit.fail("密碼錯誤");
		}
	}

	
	void logout(String sessionId){
		if (StrKit.notBlank(sessionId)) {
			CacheKit.remove(CacheConstant.SYS_ACCOUNT, sessionId);
//			Session.dao.deleteById(sessionId);
		}
	}
	
	
	/**
	 * 根据accountId 清楚账号缓存
	 * 用于系统管理员操作账号
	 */
	public void clearCacheByAccountId(Integer accountId){
		List<Session> list = Session.dao.find("select * from sys_session where accountId = ?",accountId);
		for (Session sysSession : list) {
			if (sysSession.isExpired()) {
				sysSession.delete();
			}
			CacheKit.remove(CacheConstant.SYS_ACCOUNT, sysSession.getId());
		}
	}
	
	public Account getAccountCacheWithSessionId(String sessionId) {
		return CacheKit.get(CacheConstant.SYS_ACCOUNT, sessionId);
	}
	
	
	public Account loginWithSessionId(String sessionId, String loginIp) {
		Session session = Session.dao.findById(sessionId);
		if (session == null) {      
			return null;
		}
		// 被动式删除过期数据，此外还需要定时线程来主动清除过期数据
		if (session.isExpired()) { 
			session.delete();		
			return null;
		}
		Account loginSysAccount = accountDao.findById(session.getAccountId());
		// 找到 loginSysAccount 并且 是正常状态 才允许登录
		if (loginSysAccount != null && loginSysAccount.isStatusOk()) {
			// 往Account放一份sessionId
			loginSysAccount.put("sessionId", sessionId);
			//菜单数据
			loginSysAccount.put("menus", RoleService.me.findMenuNamesByRole(loginSysAccount.getRoleId()));
			CacheKit.put(CacheConstant.SYS_ACCOUNT, sessionId, loginSysAccount);
			createLoginLog(loginSysAccount.getId(), loginIp);
			return loginSysAccount;
		}
		return null;
	}

	/**
	 * 创建登录日志
	 */
	private void createLoginLog(Integer accountId, String loginIp) {
		Record loginLog = new Record().set("accountId", accountId).set("ip", loginIp).set("loginAt", new Date());
		Db.save("sys_login_log", loginLog);
	}

}
