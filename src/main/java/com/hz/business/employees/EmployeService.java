package com.hz.business.employees;

import com.hz.common.kit.RetKit;
import com.hz.common.model.Employees;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:19
 */
public class EmployeService {

    public static final EmployeService me = new EmployeService();
    static final Employees DAO = Employees.dao;

    public Page<Employees> paginate(int pageNumber, int pageSize, Kv kv) {
        Page<Employees> page = DAO.paginate(pageNumber, pageSize, DAO.getSqlPara("employees.paginate", kv));
        return page;
    }

    public RetKit update(Employees depart) {
        return depart.update() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit save(Employees depart) {
        return depart.save() ? RetKit.ok() : RetKit.fail();
    }

    public RetKit remove(int id) {
        return DAO.deleteById(id) ? RetKit.ok() : RetKit.fail();
    }
}
