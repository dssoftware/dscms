package com.hz.business.employees;

import com.alibaba.fastjson.JSON;
import com.hz.common.controller.AdminController;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Depart;
import com.hz.common.model.Employees;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Kv;

import java.util.Date;

/**
 * @author :  Xiaoy
 * @Description :  TODO
 * @CreateTime: 2019-09-17 9:19
 */
public class EmployeController extends AdminController {

    static final EmployeService srv = EmployeService.me;

    public void index() {
        setAttr("departments",Depart.dao.findAll());
        render("/pages/personnel/employ.html");
    }

    public void list() {
        int pageSize = getParaToInt("pageSize", 10);
        int pageNumber = getParaToInt("pageNumber", 1);
        Kv kv = getParaToMap();
        renderJson(RetKit.ok("page", srv.paginate(pageNumber, pageSize, kv)));
    }
    public void save(){
        String json = HttpKit.readData(getRequest());
        Employees depart = JSON.parseObject(json, Employees.class);
        depart.setCreateAt(new Date());
        renderJson(srv.save(depart));
    }

    public void remove(){
        int id = getParamToInt("id");
        renderJson(srv.remove(id));
    }

    public void update(){
        String json = HttpKit.readData(getRequest());
        Employees depart = JSON.parseObject(json, Employees.class);
        renderJson(srv.update(depart));
    }
}
