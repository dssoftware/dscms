package com.hz.common.controller;

import com.alibaba.fastjson.JSONObject;
import com.hz.business.login.LoginService;
import com.hz.common.interceptor.AdminAuthInterceptor;
import com.hz.common.model.Account;
import com.jfinal.aop.Clear;
import com.jfinal.core.NotAction;

/**
 * @author Xiaoy
 */
public class AdminController extends BaseController {

	@Clear(AdminAuthInterceptor.class)
	public void index(){
	    Account account = LoginService.me.getAccountCacheWithSessionId(getSessionId());
		setAttr("account",account);
		render("/index.html");
	}
	
	@NotAction
	public Account getAccount(){
		return LoginService.me.getAccountCacheWithSessionId(getSessionId());
		
	}
	@NotAction
	public String getSessionId(){
		return getSession().getId();
	}

	public String getParam(String name){
		JSONObject data = JSONObject.parseObject(getRawData());
		return data.getString(name);
	}

	public Integer getParamToInt(String name){
		JSONObject data = JSONObject.parseObject(getRawData());
		return data.getIntValue(name);
	}

}
