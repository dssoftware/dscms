package com.hz.common.controller;
/**
-----------------------------------
*Describtion:前端调试通用接口
*-----------------------------------
*Creatime:2019年9月25日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:请求后参数构造规则  包名-页面名称 ：实际页面地址pages/test/test.html  参数 test-test 
*-----------------------------------
**/
public class TestController extends BaseController {

	public void index() {
		String html=getPara("html");
		render(convertUrl(html));
	}
	
	
	/**
	 * 请求解析 成对应的页面地址
	 * @return
	 */
	public  String convertUrl(String html) {
		StringBuffer sb=new StringBuffer();
		sb.append("/pages/");
		if(!"".equals(html) && html.contains("-")) {
			html=html.replace("-", "/");
			sb.append(html);
		}else {
			sb.append(html);
		}
		sb.append(".html");
		return sb.toString();
	}
}
