package com.hz.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseAccount<M extends BaseAccount<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 昵称
	 */
	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	/**
	 * 昵称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 对应部门id
	 */
	public M setDepId(java.lang.Integer depId) {
		set("dep_id", depId);
		return (M)this;
	}
	
	/**
	 * 对应部门id
	 */
	public java.lang.Integer getDepId() {
		return getInt("dep_id");
	}

	public M setRoleId(java.lang.Integer roleId) {
		set("roleId", roleId);
		return (M)this;
	}
	
	public java.lang.Integer getRoleId() {
		return getInt("roleId");
	}

	/**
	 * 账号
	 */
	public M setUsername(java.lang.String username) {
		set("username", username);
		return (M)this;
	}
	
	/**
	 * 账号
	 */
	public java.lang.String getUsername() {
		return getStr("username");
	}

	public M setSalt(java.lang.String salt) {
		set("salt", salt);
		return (M)this;
	}
	
	public java.lang.String getSalt() {
		return getStr("salt");
	}

	/**
	 * 密码
	 */
	public M setPassword(java.lang.String password) {
		set("password", password);
		return (M)this;
	}
	
	/**
	 * 密码
	 */
	public java.lang.String getPassword() {
		return getStr("password");
	}

	public M setIp(java.lang.String ip) {
		set("ip", ip);
		return (M)this;
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	/**
	 * 状态 1-正常 0-禁用
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 状态 1-正常 0-禁用
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	/**
	 * 上一次登录时间
	 */
	public M setLastLoginAt(java.util.Date lastLoginAt) {
		set("lastLoginAt", lastLoginAt);
		return (M)this;
	}
	
	/**
	 * 上一次登录时间
	 */
	public java.util.Date getLastLoginAt() {
		return get("lastLoginAt");
	}

	/**
	 * 创建时间
	 */
	public M setCreateAt(java.util.Date createAt) {
		set("createAt", createAt);
		return (M)this;
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateAt() {
		return get("createAt");
	}

	/**
	 * 更改时间
	 */
	public M setUpdateAt(java.util.Date updateAt) {
		set("updateAt", updateAt);
		return (M)this;
	}
	
	/**
	 * 更改时间
	 */
	public java.util.Date getUpdateAt() {
		return get("updateAt");
	}

}
