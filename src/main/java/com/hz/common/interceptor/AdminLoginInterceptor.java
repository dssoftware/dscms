package com.hz.common.interceptor;

import com.hz.common.constant.CacheConstant;
import com.hz.common.constant.Constant;
import com.hz.common.model.Account;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;
import org.eclipse.jetty.server.Authentication;

/**
 * 登录拦截器
 * @author Xiaoy
 */
public class AdminLoginInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller con = inv.getController();
        //判断用户是否登录
        String sid = con.getSession().getId();
        Account account = CacheKit.get(CacheConstant.SYS_ACCOUNT, sid);
        String action = inv.getActionKey();
        if (account != null || action.contains("login")) {
            inv.invoke();
        } else {
            con.redirect("/login");
        }
    }
}