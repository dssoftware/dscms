package com.hz.common.interceptor;

import com.hz.business.admin.RoleService;
import com.hz.business.login.LoginService;
import com.hz.common.constant.Constant;
import com.hz.common.constant.RetConstant;
import com.hz.common.kit.RetKit;
import com.hz.common.model.Account;
import com.hz.common.model.Menu;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import java.util.List;

/**
 * 权限验证
 */
public class AdminAuthInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller con = inv.getController();
        String sessionId = con.getSession().getId();
        Account account = LoginService.me.getAccountCacheWithSessionId(sessionId);
        if (validateAuth(account, inv.getControllerKey())) {
            inv.invoke();
        } else {
            con.renderJson(RetKit.fail(RetConstant.CODE_NO_AUTH, "你無權限訪問該路徑"));
        }
    }

    private boolean validateAuth(Account account, String controllerKey) {
        if (account.isSysAdmin()) {
            return true;
        }
        List<Menu> list = RoleService.me.findMenuIdsByRole(account.getRoleId());
        boolean isHaveAuth = false;
        for (Menu menu : list) {
            if (menu.getUrl() != null) {
                if (controllerKey.contains(menu.getUrl())) {
                    isHaveAuth = true;
                    break;
                }
            }
        }
        return isHaveAuth;
    }

}
