package com.hz.common.routes;

import com.hz.business.admin.AccountListController;
import com.hz.business.admin.RoleController;
import com.hz.business.assesment.AssesmentStandController;
import com.hz.business.assesment.AssesmentTargetController;
import com.hz.business.departments.DepartmentsController;
import com.hz.business.employees.EmployeController;
import com.hz.business.login.LoginController;
import com.hz.common.controller.AdminController;
import com.hz.common.interceptor.AdminAuthInterceptor;
import com.hz.common.interceptor.AdminLoginInterceptor;
import com.jfinal.config.Routes;

/**
 * @author :  Xiaoy
 * @Description :  路由管理
 * @CreateTime: 2019-09-15 21:20
 */
public class AdminRoutes extends Routes {

    @Override
    public void config() {
        //登陆拦截器
        this.addInterceptor(new AdminLoginInterceptor());
        //权限验证拦截器
        this.addInterceptor(new AdminAuthInterceptor());
        //管理员管理模块
        this.add("/", AdminController.class);
        this.add("/account", AccountListController.class);
        this.add("/role", RoleController.class);
        //人员管理模块
        this.add("/depart", DepartmentsController.class);
        this.add("/employ", EmployeController.class);
        //考核模块
        this.add("/standard", AssesmentStandController.class);
        this.add("/target", AssesmentTargetController.class);
        //登陆
        this.add("/login", LoginController.class);

    }
}
