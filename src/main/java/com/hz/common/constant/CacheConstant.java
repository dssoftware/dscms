package com.hz.common.constant;

public class CacheConstant {

	/**
	 * 后台管理-账号
	 */
	public final static String SYS_ACCOUNT = "SYS_ACCOUNT";
	/**
	 * 后台管理-角色菜单缓存
	 */
	public final static String SYS_ROLE_MENU = "SYS_ROLE_MENU";

	/**
	 * 登录用户缓存
	 */
	public final static String LOGIN_USER_CACHE = "LoginUserCache";
}
