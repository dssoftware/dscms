package com.hz;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.hz.common.routes.AdminRoutes;
import com.hz.common.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log4jLogFactory;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;

/**
 * @author :  Xiaoy
 * @Description :  JFinal启动配置
 * @CreateTime: 2019-09-15 19:48
 */
public class ApplicationConfig extends JFinalConfig {

    private static Prop p = loadConfig();

    private static Prop loadConfig() {
        return PropKit.use("config.txt");
    }

    /**
     * 创建Druid插件
     */
    public static DruidPlugin createDruidPlugin() {
        return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
    }

    @Override
    public void configConstant(Constants constants) {
        constants.setDevMode(p.getBoolean("devMode"));
        constants.setMaxPostSize(1024 * 1024 * 20);
        constants.setBaseUploadPath("upload/temp");
        constants.setError404View("/404.html");
        constants.setJsonDatePattern("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public void configRoute(Routes routes) {
        routes.add(new AdminRoutes());
    }

    @Override
    public void configEngine(Engine engine) {
        engine.setDevMode(p.getBoolean("devMode"));
        engine.addSharedObject("sk", new com.jfinal.kit.StrKit());
    }

    @Override
    public void configPlugin(Plugins plugins) {
        // 配置 druid 数据库连接池插件
        DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
        druidPlugin.addFilter(new StatFilter());
        WallFilter wf = new WallFilter();
        wf.setDbType("mysql");
        druidPlugin.addFilter(wf);
        plugins.add(druidPlugin);
        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        // 所有映射在 MappingKit 中自动化搞定
        arp.getEngine().setSourceFactory(new ClassPathSourceFactory());
        arp.addSqlTemplate("DB_EPM.sql");
        arp.setShowSql(Boolean.parseBoolean(PropKit.get("sqlMode")));
        arp.getSqlKit().getEngine().addSharedObject("sk", new com.jfinal.kit.StrKit());
        _MappingKit.mapping(arp);

        // 缓存
        plugins.add(new EhCachePlugin());
        plugins.add(arp);

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8888, "/", 5);
    }
}
