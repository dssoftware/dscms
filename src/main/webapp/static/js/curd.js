/**
 * 表单操作
 * 表单的提交操作最好用表单中的提交按钮来实现，不要用弹框的回调
 */
layui.define(['jquery', 'form', 'layer', 'element'], function(exports) {
  var { $, form, layer, element } = layui

  function Curd(config) {
    Object.assign(this, this.config, config || {})
    this.bindEvent()
    this.setInitVal()
  }
  Curd.prototype = {
    config: {
      addTitle: '添加',
      editTitle: '编辑',
      select: '#table-form', // form表单选择器
      submitFilter: 'submit', // 表单提交按钮的lay-filter
      formFilter: 'modify', // form的lay-filter
      addSelect: '#add', // 添加触发弹框的按钮的选择器
      bind_e: true, // 是否绑定事件
      addUrl: '', // 添加请求的url
      editUrl: '', // 编辑请求的url
      delUrl: '', // 删除请求的url
      $http: axios, // 使用的ajax工具
      index: '', // layer的index
      addResponse () {}, // 添加请求的回调
      editResponse () {}, // 编辑请求的回调
      delResponse () {}, // 删除请求的回调
      layerSuccess () {}, // layer弹出后回调
      layerEnd () {} // layer销毁后回调
    },
    setInitVal() {
      let o = {}
      let initData = $(this.select + ' [name]')
      .serializeArray()
      .reduce((o, item) => {
        o[item.name] = item.value
        return o
      }, {})
      this.initData = initData
    },
    bindEvent() {
      var t = this
      if (!t.bind_e) return

      // 添加
      $(t.addSelect).click(() => {
        t.openAdd()
        return false
      })
      form.on('submit(cancel)', function(){
        layer.close(t.index)
        return false
      })
      form.on('submit('+ t.submitFilter +')', function(data) {
        console.log(this)
        t.mode === 'add' ? t.postAdd(data.field) : t.postEdit(data.field)
        return false
      })
    },
    openAdd(data) {
      // filter表示表单的lay-filter
      this.mode = 'add'
      this.resetForm({
        data: data
      })
      this.openLayer()
    },
    openEdit(data) {
      this.mode = 'edit'
      this.parseData(data)
      this.resetForm({
        data: data
      })
      this.openLayer()
    },
    openDel(data) {
      this.postDel(data)
    },
    openLayer() {
      var t = this
      var content = $(this.select)
      this.index = layer.open({
        type: 1,
        title: t[t.mode + 'Title'],
        shadeClose: true,
        shade: [0],
        area: content.find('table').length !== 0 ? [$('body').width() + 'px', $('body').height() - 100 + 'px'] : '',
        maxmin: true, //开启最大化最小化按钮
        offset: ['100px'],
        content: content,
        success(layero) {
          t.layerSuccess()
          t.layerMakeCenter(layero)
          content.prop('style', 'display:block !important')
        },
        end() {
          t.layerEnd()
          content.prop('style', '')
          t.resetForm({})
        }
      })

      if (content.find('table').length !== 0) {
        // 弹出全屏，这里是为了让layer处于全屏打开的状态，之前已经通过area设置成全屏了
        layer.full(t.index)
      }
    },
    layerMakeCenter(layero) {
      // layer居中
      var body = layero.parent('body')
      var table = layero.find('table')
      setTimeout(() => {
        // layer设置居中的js
        layero.css({
          left: `calc(${body.width()}px / 2 - ${layero.width()}px / 2)`
        })
        if (table.length !== 0) {
          layero.find('.layui-layer-content').css({
            padding: '20px',
            'box-sizing': 'border-box'
          })
        }
      }, 0);
    },
    postAdd(data) {
      console.log(data)
      var { $http, addUrl } = this
      $http.post(addUrl, data)
        .then(res => {
          this.addResponse(res.data)
          layer.close(this.index)
        })
        .catch(err => {
          layer.msg('添加失败')
          console.log('lay-filter为' + this.formFilter + '的表单：', err)
        })
    },
    postEdit(data) {
      var { $http, editUrl } = this
      $http.post(editUrl, data)
        .then(res => {
          this.editResponse(res.data)
          layer.close(this.index)
        })
        .catch(err => {
          layer.msg('修改失败')
          console.log('lay-filter为' + this.formFilter + '的表单：', err)
        })
    },
    postDel(data) {
      var { $http, delUrl } = this
      $http.post(delUrl, data)
        .then(res => {
          this.delResponse(res.data)
        })
        .catch(err => {
          layer.msg('删除失败')
          console.log('lay-filter为' + this.formFilter + '的表单：', err)
        })
    },
    resetForm({ data }) {
      // 给lay-filter指定的表单赋值
      form.val(this.formFilter, data || this.initData)
    },
    parseData(data) {
      // 处理表单的select元素绑定值
      Object.keys(data).forEach(name => {
        let el = $(`[name=${name}]`)
        if (el.prop('tagName') === 'SELECT') {
          let options = el.prop('options')
          Array.prototype.forEach.call(options, op => {
            console.log(data[name] === op.innerText)
            if (data[name] === op.innerText) {
              data[name] = op.value
            }
          })
        }
      })
      return data
    }
  }
  exports('curd', Curd)
})
